﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnightController : MonoBehaviour 
{
	float speed = 4;
	float rotSpeed =80;
	float rot =0f;
	float gravity = 8;

	Vector3 moveDir =Vector3.zero;

	CharacterController Controller;
	Animator Anim;

	// Use this for initialization
	void Start () 
	{
		Controller = GetComponent< CharacterController> ();
		Anim = GetComponent< Animator> ();
	}

	// Update is called once per frame
	void Update ()
	{
		if (Controller.isGrounded) {
			if (Input.GetKey (KeyCode.W)) {
				if (Anim.GetBool ("Attaking") == true) {
					return;
				} 
				else if (Anim.GetBool ("Attaking") == false) 
				{
					Anim.SetBool ("running", true);
					Anim.SetInteger ("condition", 1);
					moveDir = new Vector3 (0, 0, 1);
					moveDir *= speed;
					moveDir = transform.TransformDirection (moveDir);
				}
			}
			if (Input.GetKeyUp (KeyCode.W)) {
				Anim.SetBool ("attack", false);
				Anim.SetInteger ("condition", 0);
				moveDir = new Vector3 (0, 0, 0);
			}
		}
		GetInput ();
		rot += Input.GetAxis ("Horizontal") * rotSpeed * Time.deltaTime;
		transform.eulerAngles = new Vector3 (0, rot, 0);
		moveDir.y -= gravity * Time.deltaTime;
		Controller.Move (moveDir * Time.deltaTime);
	}
	void GetInput()
	{
		if (Controller.isGrounded) { 
			if (Input.GetMouseButtonDown (0)) {
				if (Anim.GetBool ("running") == true) {
					Anim.SetBool ("running", false);
					Anim.SetInteger ("condition", 0);
				}

				if (Anim.GetBool ("running") == false) {
					Attacking ();
				}
			}
		}
	}
	void Attacking ()
	{
		StartCoroutine (AttackRoutine());

	}

	IEnumerator AttackRoutine()
	{
		Anim.SetBool("Attacking", true);
		Anim.SetInteger("condition", 2);
		yield return new WaitForSeconds (1);
		Anim.SetInteger("condition", 0);
		Anim.SetBool("Attacking", false);
	}

}
